#!/usr/bin/perl
# SPDX-License-Identifier: GPL-2.0-or-later

# This script behaves like Dpkg::Arch(3perl) debarch_is_concerned().
#
# Replace this script with python-debian architecture_is_concerned()
# once python3-debian (>= 0.1.48) can be assumed.

use strict;
use warnings;

use Dpkg::Arch qw(debarch_is_concerned);

if (@ARGV != 2) {
    die "Usage: $0 <arch> <restrictions>\n";
}

my ($arch, $restrictions) = @ARGV;

if (debarch_is_concerned($arch, split /[\s,]+/, $restrictions)) {
    exit 0;
}

exit 1;
